OS := \n\n
ifeq (, Darwin)\n
  CUNIT_PATH_PREFIX = /usr/local/Cellar/cunit/2.1-3/\n
  CUNIT_DIRECTORY = cunit\n
endif\n
ifeq (, Linux)\n
  CUNIT_PATH_PREFIX = /util/CUnit/\n
  CUNIT_DIRECTORY = CUnit/\n
endif\n\n
OBJECTS = 2.o\n
EXECUTABLE = 3\n\n
.PHONY: clean\n
clean:\n
<TAB>rm -rf *~ *.o  *.xml *.gc?? *.dSYM\n
